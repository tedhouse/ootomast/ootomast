FROM php:7-apache

RUN apt-get update
RUN docker-php-ext-install mysqli

COPY . /var/www/

RUN mv /var/www/000-default.conf /etc/apache2/sites-available/000-default.conf
RUN mv /var/www/start-apache /usr/local/bin

RUN chown -R www-data:www-data /var/www

RUN a2enmod rewrite

CMD ["start-apache"]
